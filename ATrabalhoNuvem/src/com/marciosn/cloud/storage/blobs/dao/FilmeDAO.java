package com.marciosn.cloud.storage.blobs.dao;

import com.marciosn.cloud.storage.blobs.model.Filme;

public interface FilmeDAO extends GenericDAO<Filme>{

}
