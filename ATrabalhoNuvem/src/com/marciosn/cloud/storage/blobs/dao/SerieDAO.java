package com.marciosn.cloud.storage.blobs.dao;

import com.marciosn.cloud.storage.blobs.model.Serie;

public interface SerieDAO extends GenericDAO<Serie>{

}
