package com.marciosn.cloud.storage.blobs.login;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import sun.misc.Cleaner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.marciosn.cloud.storage.blobs.dao.UsuarioJPADAO;
import com.marciosn.cloud.storage.blobs.model.Filme;
import com.marciosn.cloud.storage.blobs.model.Usuario;

/**
 * @author Egila Karen
 *
 */
@ManagedBean
@RequestScoped
public class Login implements Serializable{

	private static final long serialVersionUID = -3984002432649039239L;
	private String username;
	private String password;
	private UsuarioJPADAO usuarioDAO = new UsuarioJPADAO();
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	String n = "M�rcio Souza";

	public Login(){
		HttpSession s = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if(s != null){
			s.invalidate();
		}
	}
	
//	public String loginBean(){
//		System.out.println("Entrou em LoginBean");
//		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//		try{
//			for(Usuario u : getUsuarioBanco()){
//				if(u.getNome().contains(username)){
//					if(session == null){
//						session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
//					}
//					session.setAttribute("username", username);
//					return "/index?faces-redirect=true";
//				}else{
//					if(session != null){
//						session.invalidate();
//					}
//				}
//			}	
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return "/login";
//		
//		
//	}
	
	public List<Usuario> getUsuarioBanco(){
		List<Usuario> usuarios = usuarioDAO.find();
		return usuarios;
	}
	
	@SuppressWarnings("deprecation")
	public String autenticarUsuario(String email, String senha){
		System.out.println("Entrou no metodo autenticarUsuario");
		System.out.println("email: " + email + " " + senha);
		
		@SuppressWarnings("resource")
		HttpClient httpCliente = new DefaultHttpClient();
		HttpUriRequest httpUriRequest = new HttpGet("http://138.91.120.126:8080/WSBandasRest/usuario/getAutentica/" + email + "/" + senha);
		
		Usuario usuario = null;
		try {
			HttpResponse httpResponse = httpCliente.execute(httpUriRequest);
			InputStream inputStream = httpResponse.getEntity().getContent();
			Reader reader = new InputStreamReader(inputStream);
			
			Gson gson = new Gson();
			
			usuario = gson.fromJson(reader, Usuario.class);
			return "/index?faces-redirect=true";
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return "/login";
		
//		HttpPost post = new HttpPost("http://138.91.120.126:8080/WSBandasRest/usuario/getAutentica");
//		
//		List<NameValuePair> parametros = new ArrayList<NameValuePair>();
//		parametros.add(new BasicNameValuePair("email", email));
//		parametros.add(new BasicNameValuePair("senha", senha));
//		
//		Usuario usuario = null;
//		try {
//			post.setEntity(new UrlEncodedFormEntity(parametros));
//			HttpResponse response = cliente.execute(post);	
//			InputStream inputStream = response.getEntity().getContent();
//			Reader reader = new InputStreamReader(inputStream);
//			
//			Gson gson = new Gson();
//			
//			usuario = gson.fromJson(reader, Usuario.class);
//			return "/index?faces-redirect=true";
//			
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return "/login";
	}
	
	/**
	 * Get e Set
	 * */
	public String getNome() {
		return username;
	}
	public void setNome(String nome) {
		this.username = nome;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UsuarioJPADAO getUsuarioDAO() {
		return usuarioDAO;
	}
	public void setUsuarioDAO(UsuarioJPADAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
}
